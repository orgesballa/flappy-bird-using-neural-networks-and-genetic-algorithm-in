from sketch import *


def resetGame():
    counter = 0
    if bestBird:
        bestBird.score = 0

    pipes = []


def nextGeneration():
    resetGame()
    normalizeFitness(allBirds)
    activeBirds = generate(allBirds)
    allbirds = list_splice(activeBirds, 0)


def generate(oldBirds):
    newBirds = []
    for i in range(len(oldBirds)):
        bird = poolSelection(oldBirds)
        newBirds[i] = bird

    return newBirds


def normalizeFitness(birds):
    for i in range(len(birds)):
        birds[i].score = birds[i].score ** 2

    sum = 0
    for i in range(len(birds)):
        sum += birds[i].score

    for i in range(len(birds)):
        birds[i].fitness = birds[i].score / sum


def poolSelection(birds):
    index = 0
    r = random.random(0, 1)

    while r > 0:
        r -= birds[index].fitness
        index += 1

    index -= 1

    return birds[index].copy()
