from nn import *
import pygame
from pygame import Rect

width = 284 * 2
height = 512
FPS = 60


class Bird(pygame.sprite.Sprite):
    w = 32
    h = 32
    SINK_SPEED = 0.1
    CLIMB_SPEED = 0.25
    CLIMB_DURATION = 200

    def __init__(self, brain, msec_to_climb, images):
        super(Bird, self).__init__()
        self.x = 64
        self.y = height / 2
        self.r = 12

        self.gravity = 0.2
        self.lift = -12
        self.velocity = 0
        self.msec_to_climb = msec_to_climb
        if isinstance(brain, NeuralNetwork):
            self.brain = brain.copy()
            self.brain.mutate()
        else:
            self.brain = NeuralNetwork(5, 8, 2)

        self.score = 0
        self.fitness = 0
        self.images = images
        self._img_wingup, self._img_wingdown = images
        self._mask_wingup = pygame.mask.from_surface(self._img_wingup)
        self._mask_wingdown = pygame.mask.from_surface(self._img_wingdown)

    def copy(self, msec_to_climb, images):
        return Bird(self.brain, msec_to_climb, images)

    def think(self, pipes):
        closest = None
        record = 999999999
        for i in range(len(pipes)):
            diff = pipes[i].x - self.x
            if diff > 0 and diff < record:
                record = diff
                closest = pipes[i]
            # closest = pipes[0]
        if closest is not None:
            inputs = []
            dist =  closest.x - self.x
            print(closest.x, closest.top, closest.bottom, self.y, dist)
            inputs.append(closest.x)
            inputs.append(closest.top)
            inputs.append(closest.bottom)
            inputs.append(self.y)
            inputs.append(dist)
            action = self.brain.predict(inputs)
            # print(action)
            if action[1] >= action[0]:
                self.up()

    @property
    def rect(self):
        """Get the bird's position, width, and height, as a pygame.Rect."""
        return Rect(self.x, self.y, Bird.w, Bird.h)

    @property
    def image(self):
        """Get a Surface containing this bird's image.

        This will decide whether to return an image where the bird's
        visible wing is pointing upward or where it is pointing downward
        based on pygame.time.get_ticks().  This will animate the flapping
        bird, even though pygame doesn't support animated GIFs.
        """
        if pygame.time.get_ticks() % 500 >= 250:
            return self._img_wingup
        else:
            return self._img_wingdown

    def up(self):
        self.msec_to_climb += Bird.CLIMB_DURATION

    def bottomTop(self):
        return (self.y > height or self.y < 0)

    def update(self, delta_frames=1):
        if self.msec_to_climb > 0:
            frac_climb_done = 1 - self.msec_to_climb / Bird.CLIMB_DURATION
            self.y -= (Bird.CLIMB_SPEED * frames_to_msec(delta_frames) *
                       (1 - math.cos(frac_climb_done * math.pi)))
            self.msec_to_climb -= frames_to_msec(delta_frames)
        else:
            self.y += Bird.SINK_SPEED * frames_to_msec(delta_frames)
        # self.velocity += self.gravity
        # self.y += -7
        # self.x += 0.1
        self.score += 1


def frames_to_msec(frames, fps=FPS):
    """Convert frames to milliseconds at the specified framerate.

    Arguments:
    frames: How many frames to convert to milliseconds.
    fps: The framerate to use for conversion.  Default: FPS.
    """
    return 1000.0 * frames / fps


def msec_to_frames(milliseconds, fps=FPS):
    """Convert milliseconds to frames at the specified framerate.

    Arguments:
    milliseconds: How many milliseconds to convert to frames.
    fps: The framerate to use for conversion.  Default: FPS.
    """
    return fps * milliseconds / 1000.0
