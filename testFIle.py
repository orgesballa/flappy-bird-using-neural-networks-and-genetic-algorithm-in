import numpy as np
import math
import pygame
import os
import sys
import random


def randomize(arr):
    for i in range(np.size(arr, 0)):
        for j in range(np.size(arr, 1)):
            arr[i][j] = random.random() * 2 - 1
    return arr


input_array = [0.2, 0.4, 0.7, 1, 4]
np.matrix(input_array)
inputs = np.matrix(input_array).T
# print(inputs)
weights_ih = np.zeros((8, 5))
weights_ho = np.zeros((2, 8))
bias_h = np.zeros((8, 1))
bias_o = np.zeros((8, 1))
bias_o = randomize(bias_o)
bias_h = randomize(bias_h)
weights_ih = randomize(weights_ih)
weights_ho = randomize(weights_ho)
# print(bias_h)
hidden = np.dot(weights_ih, inputs)
# print(hidden)
# print(np.size(hidden, 0))
# print(np.size(bias_h,0))
hidden = np.add(hidden, bias_h)
# print(hidden)
for i in range(np.size(hidden, 0)):
    for j in range(np.size(hidden, 1)):
        hidden[i][j] = 1 / (1 + math.exp(-hidden[i][j]))
print(hidden)
output = np.dot(weights_ho, hidden)
for i in range(np.size(output, 0)):
    for j in range(np.size(output, 1)):
        output[i][j] = 1 / (1 + math.exp(-output[i][j]))
# print(output)

# def main():
#     pygame.init()
#     display_surface = pygame.display.set_mode((284*2, 512))
#     pygame.display.set_caption('Pygame Flappy S')
#     clock = pygame.time.Clock()
#     score_font = pygame.font.SysFont(None, 32, bold=True)  # default font
#     images = load_images()
#     pygame.display.update()
#     while True:
#         clock.tick(60)
#         # display_surface.fill([0, 0, 0])
#         pygame.display.flip()
#         bg = pygame.image.load(os.path.join("images", "background.png"))
#         display_surface.blit(bg, (0, 0))
#         for evt in pygame.event.get():
#             if evt.type == pygame.QUIT:
#                 pygame.quit()
#                 sys.exit()
#
#
#
# def load_images():
#     """Load all images required by the game and return a dict of them.
#
#     The returned dict has the following keys:
#     background: The game's background image.
#     bird-wingup: An image of the bird with its wing pointing upward.
#         Use this and bird-wingdown to create a flapping bird.
#     bird-wingdown: An image of the bird with its wing pointing downward.
#         Use this and bird-wingup to create a flapping bird.
#     pipe-end: An image of a pipe's end piece (the slightly wider bit).
#         Use this and pipe-body to make pipes.
#     pipe-body: An image of a slice of a pipe's body.  Use this and
#         pipe-body to make pipes.
#     """
#
#     def load_image(img_file_name):
#         """Return the loaded pygame image with the specified file name.
#
#         This function looks for images in the game's images folder
#         (./images/).  All images are converted before being returned to
#         speed up blitting.
#
#         Arguments:
#         img_file_name: The file name (including its extension, e.g.
#             '.png') of the required image, without a file path.
#         """
#         file_name = os.path.join('.', 'images', img_file_name)
#         img = pygame.image.load(file_name)
#         img.convert()
#         return img
#
#     return {'background': load_image('background.png'),
#             'pipe-end': load_image('pipe_end.png'),
#             'pipe-body': load_image('pipe_body.png'),
#             # images for animating the flapping bird -- animated GIFs are
#             # not supported in pygame
#             'bird-wingup': load_image('bird_wing_up.png'),
#             'bird-wingdown': load_image('bird_wing_down.png')}
#
#
# if __name__ == '__main__':
#     # If this module had been imported, __name__ would be 'flappybird'.
#     # It was executed (e.g. by double-clicking the file), so call main.
#     main()
