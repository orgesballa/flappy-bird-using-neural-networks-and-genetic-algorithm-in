import random
import pygame
from bird import *
from pygame import Rect

width = 284 * 2
height = 512
w = 80


class Pipe(pygame.sprite.Sprite):
    width = 284 * 2
    height = 512
    w = 80
    piece_height = 32

    def __init__(self, pipe_end_img, pipe_body_img):

        spacing = 20
        centery = random.randint(spacing, height - spacing)

        self.top = centery - spacing / 2
        self.bottom = height - (centery + spacing / 2)

        self.x = width
        self.w = 80
        self.speed = 3
        self.piece_height = 32

        self.image = pygame.Surface((Pipe.w, height), 65536)
        self.image.convert()  # speeds up blitting
        self.image.fill((0, 0, 0, 0))
        total_pipe_body_pieces = int(
            (height -  # fill window from top to bottom
             3 * Pipe.piece_height -  # make room for bird to fit through
             3 * Pipe.piece_height) /  # 2 end pieces + 1 body piece
            Pipe.piece_height  # to get number of pipe pieces
        )
        self.bottom_pieces = random.randint(1, total_pipe_body_pieces)
        self.top_pieces = total_pipe_body_pieces - self.bottom_pieces

        # bottom pipe
        for i in range(1, self.bottom_pieces + 1):
            piece_pos = (0, height - i * Pipe.piece_height)
            self.image.blit(pipe_body_img, piece_pos)
        bottom_pipe_end_y = height - self.bottom_height_px
        bottom_end_piece_pos = (0, bottom_pipe_end_y - Pipe.piece_height)
        self.image.blit(pipe_end_img, bottom_end_piece_pos)

        # top pipe
        for i in range(self.top_pieces):
            self.image.blit(pipe_body_img, (0, i * Pipe.piece_height))
        top_pipe_end_y = self.top_height_px
        self.image.blit(pipe_end_img, (0, top_pipe_end_y))
        self.mask = pygame.mask.from_surface(self.image)

    @property
    def bottom_height_px(self):
        """Get the bottom pipe's height, in pixels."""
        return self.bottom_pieces * Pipe.piece_height

    @property
    def top_height_px(self):
        """Get the top pipe's height, in pixels."""
        return self.top_pieces * Pipe.piece_height

    @property
    def rect(self):
        """Get the Rect which contains this PipePair."""
        return Rect(self.x, 0, Pipe.w, Pipe.piece_height)

    def hits(self, bird):
        if ((bird.y - bird.r) < self.top or (bird.y + bird.r) > (height - self.bottom)):
            if (bird.x > self.x and bird.x < self.x + self.w):
                return True

        return False

    def update(self):
        self.x -= self.speed

    def offscreen(self):
        if self.x < -self.w:
            return True
        else:
            return False

    def collides_with(self, bird):
        """Get whether the bird collides with a pipe in this PipePair.

        Arguments:
        bird: The Bird which should be tested for collision with this
            PipePair.
        """
        return pygame.sprite.collide_mask(self, bird)

    def visible(self):
        return -Pipe.w < self.x < width
