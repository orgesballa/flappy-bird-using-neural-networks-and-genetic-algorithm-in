import numpy as np
import random
import math


def randomize(arr):
    for i in range(np.size(arr, 0)):
        for j in range(np.size(arr, 1)):
            arr[i][j] = random.random() * 2 - 1
    return arr


class NeuralNetwork:
    def __init__(self, in_nodes, hid_nodes, out_nodes):
        if isinstance(in_nodes, NeuralNetwork):
            a = in_nodes
            self.input_nodes = a.input_nodes
            self.hidden_nodes = a.hidden_nodes
            self.output_nodes = a.output_nodes

            self.weights_ih = np.empty_like(a.weights_ih)
            self.weights_ih[:] = a.weights_ih

            self.weights_ho = np.empty_like(a.weights_ho)
            self.weights_ho[:] = a.weights_ho

            self.bias_h = np.empty_like(a.bias_h)
            self.bias_h[:] = a.bias_h
            self.bias_o = np.empty_like(a.bias_o)
            self.bias_o[:] = a.bias_o


        else:
            self.input_nodes = in_nodes
            self.hidden_nodes = hid_nodes
            self.output_nodes = out_nodes

            self.weights_ih = np.zeros((self.hidden_nodes, self.input_nodes))
            self.weights_ho = np.zeros((self.output_nodes, self.hidden_nodes))
            self.weights_ih = randomize(self.weights_ih)
            self.weights_ho = randomize(self.weights_ho)

            self.bias_h = np.zeros((self.hidden_nodes, 1))
            self.bias_o = np.zeros((self.output_nodes, 1))
            self.bias_o = randomize(self.bias_o)
            self.bias_h = randomize(self.bias_h)

        self.setLearningRate()

    def predict(self, input_array):
        inputs = np.matrix(input_array).T
        hidden = np.dot(self.weights_ih, inputs)
        hidden = np.add(hidden, self.bias_h)

        for i in range(np.size(hidden)):
            for j in range(1):
                if hidden[i][j] < 0:
                    hidden[i][j] = 1 - 1 / (1 + math.exp(hidden[i][j]))
                else:
                    hidden[i][j] = 1 / (1 + math.exp(-hidden[i][j]))

        output = np.dot(self.weights_ho, hidden)
        for i in range(np.size(output, 0)):
            for j in range(np.size(output, 1)):
                output[i][j] = 1 / (1 + math.exp(-output[i][j]))

        return output

    def setLearningRate(self, learning_rate=0.1):
        self.learning_rate = learning_rate

    def copy(self):
        return NeuralNetwork(self, hid_nodes=8, out_nodes=2)

    def mutate(self):
        for i in range(np.size(self.weights_ih, 0)):
            for j in range(np.size(self.weights_ih, 1)):
                if random.random() < 0.1:
                    offset = random.gauss(0, 1) * 0.5
                    self.weights_ih[i][j] = self.weights_ih[i][j] + offset
        for i in range(np.size(self.weights_ho, 0)):
            for j in range(np.size(self.weights_ho, 1)):
                if random.random() < 0.11:
                    offset = random.gauss(0, 1) * 0.5
                    self.weights_ho[i][j] = self.weights_ho[i][j] + offset

        for i in range(np.size(self.bias_h, 0)):
            for j in range(np.size(self.bias_h, 1)):
                if random.random() < 0.1:
                    offset = random.gauss(0, 1) * 0.5
                    self.bias_h[i][j] = self.bias_h[i][j] + offset

        for i in range(np.size(self.bias_o, 0)):
            for j in range(np.size(self.bias_o, 1)):
                if random.random() < 0.1:
                    offset = random.gauss(0, 1) * 0.5
                    self.bias_o[i][j] = self.bias_o[i][j] + offset
