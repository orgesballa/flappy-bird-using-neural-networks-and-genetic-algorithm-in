import pygame
import os
from bird import *
from nn import *
from pipe import *
from pygame.locals import *

totalPopulation = 500

# global activeBirds
# global allBirds
# global pipes
counter = 0
width = 284 * 2
height = 512
global highScore
FPS = 60


def list_splice(target, start, delete_count=None, *items):
    """Remove existing elements and/or add new elements to a list.

    target        the target list (will be changed)
    start         index of starting position
    delete_count  number of items to remove (default: len(target) - start)
    *items        items to insert at start index

    Returns a new list of removed items (or an empty list)
    """
    if delete_count == None:
        delete_count = len(target) - start

    # store removed range in a separate list and replace with *items
    total = start + delete_count
    removed = target[start:total]
    target[start:total] = items

    return removed


def main():
    # global pipes, highScore, bestBird
    global activeBirds
    global allBirds
    global pipes
    bestBird = None
    highScore = 0
    global counter
    pipes = []
    pygame.init()

    display_surface = pygame.display.set_mode((width, height))
    pygame.display.set_caption('Pygame Flappy Bird')
    clock = pygame.time.Clock()
    score_font = pygame.font.SysFont(None, 32, bold=True)  # default font
    images = load_images()

    #     Create Population
    def nextGeneration(allBirds):
        global counter
        counter = 0
        global pipes
        pipes = []
        normalizeFitness(allBirds)
        activeBirds = generate(allBirds)

        # print('new generation', activeBirds)
        return activeBirds

    def generate(oldBirds):
        newBirds = []
        # for i in range(len(oldBirds)):
        #     bird = poolSelection(oldBirds)
        #     newBirds.append(bird)
        newBirds = poolSelection(oldBirds)
        for i in range(250):
            newBirds.append(Bird(None, 2, (images['bird-wingup'], images['bird-wingdown'])))
        return newBirds

    def normalizeFitness(birds):
        for i in range(len(birds)):
            birds[i].score = birds[i].score * 2
            # print(birds[i].score)
        sum = 0
        for i in range(len(birds)):
            sum += birds[i].score

        for i in range(len(birds)):
            if sum != 0:
                birds[i].fitness = birds[i].score / sum
                # print((birds[i].fitness))

    def poolSelection(birds):
        # index = 0
        #         # r = random.random()
        birds.sort(key=lambda x: x.score, reverse=True)
        # while r > 0:
        #     r -= birds[index].fitness
        #     index += 1
        #
        # index -= 1
        # bestBird2 = birds[0]
        # newBirds = []
        # print(len(birds))
        #
        # for i in range(len(birds)):
        #     bird = Bird(bestBird2.brain, bestBird2.msec_to_climb, bestBird2.images)
        #     newBirds.append(bird)
        lst = birds[:len(birds) - 250]
        # for i in range(len(lst)):
        #     print(lst[i].fitness)
        return lst

    activeBirds = []
    allBirds = []
    for i in range(totalPopulation):
        bird = Bird(None, 2, (images['bird-wingup'], images['bird-wingdown']))
        activeBirds.append(bird)
        allBirds.append(bird)
        # display_surface.blit(bird.image, bird.rect)
    while True:
        clock.tick(60)

        # pipes.append(Pipe(images['pipe-end'], images['pipe-body']))
        # display_surface.blit(pipes[0].image, pipes[0].rect)

        for x in (0, width / 2):
            display_surface.blit(images['background'], (x, 0))
        # print(len(pipes), 'pipes')
        for i in range(len(pipes) - 1, -1, -1):
            pipes[i].update()
            if not pipes[i].visible():
                pipes.pop(i)

        # print(len(activeBirds))
        i = 0
        for bird in activeBirds:
            # bird = activeBirds[i]
            # print(i)
            i += 1
            for evt in pygame.event.get():
                if evt.type == pygame.QUIT:
                    pygame.quit()
                    quit()
            bird.think(pipes)
            bird.update()
            if (bird.bottomTop()):
                activeBirds.remove(bird)
                continue

            for j in range(len(pipes)):
                if pipes[j].collides_with(bird):
                    activeBirds.remove(bird)
                    break

        # print('passed')
        if counter % 175 == 0:
            pipes.append(Pipe(images['pipe-end'], images['pipe-body']))
        counter += 1

        for i in range(len(pipes)):
            display_surface.blit(pipes[i].image, pipes[i].rect)

        for i in range(len(activeBirds)):
            display_surface.blit(activeBirds[i].image, activeBirds[i].rect)

        tempHighScore = 0
        tempBestBird = None

        for i in range(len(activeBirds)):
            s = activeBirds[i].score
            if s > tempHighScore:
                tempHighScore = s
                tempBestBird = activeBirds[i]
            if tempHighScore > highScore:
                highScore = tempHighScore
                bestBird = tempBestBird
        # print('Length of Active Birds', len(activeBirds))
        if len(activeBirds) == 0:
            activeBirds = nextGeneration(allBirds)

        # score_surface = score_font.render(str(bestBird.score), True, (255, 255, 255))
        # score_x = width / 2 - score_surface.get_width() / 2
        # display_surface.blit(score_surface, (score_x, Pipe.y))
        pygame.display.flip()


def load_images():
    """Load all images required by the game and return a dict of them.

    The returned dict has the following keys:
    background: The game's background image.
    bird-wingup: An image of the bird with its wing pointing upward.
        Use this and bird-wingdown to create a flapping bird.
    bird-wingdown: An image of the bird with its wing pointing downward.
        Use this and bird-wingup to create a flapping bird.
    pipe-end: An image of a pipe's end piece (the slightly wider bit).
        Use this and pipe-body to make pipes.
    pipe-body: An image of a slice of a pipe's body.  Use this and
        pipe-body to make pipes.
    """

    def load_image(img_file_name):
        """Return the loaded pygame image with the specified file name.

        This function looks for images in the game's images folder
        (./images/).  All images are converted before being returned to
        speed up blitting.

        Arguments:
        img_file_name: The file name (including its extension, e.g.
            '.png') of the required image, without a file path.
        """
        file_name = os.path.join('.', 'images', img_file_name)
        img = pygame.image.load(file_name)
        img.convert()
        return img

    return {'background': load_image('background.png'),
            'pipe-end': load_image('pipe_end.png'),
            'pipe-body': load_image('pipe_body.png'),
            # images for animating the flapping bird -- animated GIFs are
            # not supported in pygame
            'bird-wingup': load_image('bird_wing_up.png'),
            'bird-wingdown': load_image('bird_wing_down.png')}


# def resetGame(pipes):
#     # global bestBird, pipes, counter
#     counter = 0
#     # if bestBird:
#     #     bestBird.score = 0
#     pipes = []
#     return pipes


# def nextGeneration(allBirds, pipes):
#     resetGame(pipes)
#     normalizeFitness(allBirds)
#     activeBirds = generate(allBirds)
#     allbirds = list_splice(activeBirds, 0)
#     return allBirds


# class Matrix:
#     def __init__(self, rows, cols):
#         self.rows = rows
#         self.cols = cols
#         self.data = np.zeros(rows, cols)
#
#     def __copy__(self):
#         m = Matrix(self.rows, self.cols)
#         for i in range(self.rows):
#             for j in range(self.cols):
#                 m.data[i][j] = self.data[i][j]
#         return m
#
#     def copy2(self):
#         m =
#     # @staticmethod
#     # def fromArray(arr):
#     #
#     # # tsodo
#
#     # @staticmethod
#     # def substract(a, b):
#     #     if a.rows != b.rows or a.cols != b.cols:
#     #         return None
#     #     # return Matrix(a.rows,a.cols).map((_,i,j) => a.data[i][j] - b.data[i][j])
#
#     def toArray(self):
#         arr = []
#         for i in range(self.rows):
#             for j in range(self.cols):
#                 arr.append(self.data[i][j])
#
#         return arr
#
#     def randomize(self):
#         for i in range(self.rows):
#             for j in range(self.cols):
#                 self.data[i][j] = random.random(0, 1) * 2 - 1
#
#         return self
#
#     def add(self, n):
#         if isinstance(n, Matrix):
#             if self.rows != n.rows or self.cols != n.cols:
#                 return None
#         for i in range(self.rows):
#             for j in range(self.cols):
#                 self.data[i][j] += n.data[i][j]
#
#         return self
#
#     # def m(self,matrix):
#     #     return Matrix(matrix.cols,matrix.rows).map()
#
#     @staticmethod
#     def transpose(matrix):
#         rez = Matrix(matrix.cols, matrix.rows)
#         rez.data = [[matrix.data[j][i] for j in range(len(matrix.rows))] for i in range(len(matrix.cols))]
#         return rez
#
#     @staticmethod
#     def multiply(a,b):
#         if a.cols != b.rows:
#             return None
#
#
#
#     def map(self, func):
#         for i in range(self.rows):
#             for j in range(self.cols):
#                 val = self.data[i][j]
#                 self.data[i][j] = func(val, i, j)
#
#         return self
#
#     # @staticmethod
#     # def map(matrix,func):
#     #     return Matrix(matrix.rows,matrix.cols).map((e,i,j) : func(matrix.data[i][j],i,j))
#
#     def print(self):
#         print(self.data)
#
#     def serialize(self):
#         return json.dumps(self)

if __name__ == '__main__':
    # If this module had been imported, __name__ would be 'flappybird'.
    # It was executed (e.g. by double-clicking the file), so call main.
    main()
